package deber;
//crea una estructura de datos para luego con una clase interna iterator
//poder  imporimirla 

public class DataStructure {

	//create an array
	
	private final static int SIZE = 15;
	
	private int[] arrayOfInts = new int[SIZE];
	
	public DataStructure() {
	
	//fill the array with ascending integer values
	
	for (int i = 0; i < SIZE; i++) {
	
	arrayOfInts[i] = i;
	
	}
	}
	public void printEven() {
		//print out values of even indices of the array
		InnerEvenIterator iterator = this.new InnerEvenIterator();
		while (iterator.hasNext()) {
		System.out.println(iterator.getNext() + " ");
	
	}
	}

	private class InnerEvenIterator {
		//Contador, que empieza en la posici�n 0
		private int next = 0;
		public boolean hasNext() {
		//Comprueba si el elemento actual es el �ltimo dentro del array
		return (next <= SIZE - 1);
		}	public int getNext() {
		//Registra un valor dentro del �ndice par del array.
		int retValue = arrayOfInts[next];
		//Obtiene el siguiente elemento y lo devuelve
		next += 2;
		return retValue;
	}
	
	}}

