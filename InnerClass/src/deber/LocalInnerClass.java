package deber;

public class LocalInnerClass {
//crea una clase dentro de un metodo y lo instancia dentro del mismo metodo
	//en este caso despliega el dato 30
	 private int data=30;//instance variable
	 void display(){
	  class Local{
	   void msg(){System.out.println(data);}
	  }
	  Local l=new Local();
	  l.msg();
	 }
	
}
