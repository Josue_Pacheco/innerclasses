package deber;

public class Aplicacion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DataStructure ds = new DataStructure();
		ds.printEven();
		
		LocalInnerClass obj=new LocalInnerClass();
		obj.display();
		//anonymou pizza
		Pizza p = new Pizza(){
		      public void eat()
		      {
		         System.out.println("anonymous pizza");
		      }
		   };

		/////////////////
		 //  crea una clase estatica dentro de la clase pizza 
		   Pizza.Nested pn = new Pizza.Nested();
		   pn.print();
	}

}
